<?php
include "header.php";
include "menu.php";
if (!isset($_SESSION['user']))
  header("Location: /index.php?err=you_must_be_logged_in");
?>
<div class="container">
  <div class="row">
    <div class="col-sm">
      <div>
        <img src="" id="overlayImg" style="position: absolute;">
        <video id="video">No streaming available...</video>
        </div>
    </div>
    <div class="col-sm">
      <button id="photo-button" class="btn btn-dark">Take Photo</button>
      <button id="clear-button" class="btn btn-light">Clear</button>
      <button id="publish-button" class="btn btn-light">Publish</button>
      <canvas id="canvas">
      </canvas>
    </div>
  </div>
  <div class="row">
    <?php
    if ($handle = opendir('./imgs'))
    {
      while (false !== ($entry = readdir($handle)))
      {
        if ($entry[0] !== '.')
        {
          echo '<div class="col-sm">';
          echo '<img onclick="overlay(this);" src="./imgs/'.$entry.'">';
          echo '</div>';
        }
      }
    }
    ?>
  </div>
  
</div>
<div class="container">
  <div id="photos">
  </div>
</div>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/script.js"></script>