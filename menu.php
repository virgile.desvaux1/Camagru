<div class="container">
  <div class="row topmenu">
    <div class="col-sm menucol">
        <a href="/">Index</a>
    </div>
    
    <?php
    if (isset($_SESSION['user']))
    {
        echo '<div class="col-sm menucol"><a href="/account.php">Account</a></div>';
        echo '<div class="col-sm menucol"><a href="/logout.php">Logout</a></div>';
    }
    else
    {
        echo '<div class="col-sm menucol"><a href="/register.php">Register</a></div>';
        echo '<div class="col-sm menucol"><a href="/login.php">Login</a></div>';
    }
    ?>
  </div>
</div>