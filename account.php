<?php
include "header.php";
include "menu.php";
if (!isset($_SESSION['user']))
    header("Location: /login.php?err=you_must_be_logged_in");
else
{
    include 'database.php';
    $bdd = connectDB();
    $stmt = $bdd->prepare("SELECT * FROM user WHERE username='".$_SESSION['user']."'");
    $stmt->execute();
    $query = $stmt->fetch();
    print_r($query);
    $stmt = $bdd->prepare("SELECT COUNT(*) FROM posts WHERE  username='".$_SESSION['user']."'");
    $stmt->execute();
    $postCount = $stmt->fetch()[0];
}
?>
<title>Account</title>

<div class="container">
  <div class="row">
    <div class="col-sm">
        Hello, <?php echo $_SESSION['user']; ?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
        Email: <?php echo $query['email']; ?>
    </div>
    <div class="col-sm">
        <a href="/change_email.php">Change email</a>
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
        Posts: <?php echo $postCount; ?>
    </div>
    <div class="col-sm">
        <a href="/show_posts.php?<?php echo $query['id']; ?>">Show Posts</a>
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
        Comments: <?php echo $query['comments']; ?>
    </div>
    <div class="col-sm">
        <a href="/show_comments.php?<?php echo $query['id']; ?>">Show Comments</a>
    </div>
  </div>
</div>