<?php
session_start();
include 'database.php';
if (filter_var($_POST['InputEmail'], FILTER_VALIDATE_EMAIL))
{
    if (strlen($_POST['InputPassword']) > 5)
    {
        if (strcmp($_POST['InputPassword'], $_POST['InputPasswordConfirm']) == 0)
        {
            if(preg_match('/^[a-zA-Z0-9]{5,}$/', $_POST['InputUsername']))
            {
                $bdd = connectDB();
                $stmt = $bdd->prepare("SELECT * FROM user WHERE username='".$_POST['InputUsername']."'");
                $stmt->execute();
                $query = $stmt->fetch();
                echo $query;
                if ($query)
                {
                    if (strcmp($query['username'], $_POST['InputUsername']) == 0)
                    {
                        header("Location: /register.php?err=username_already_in_use");
                        return ;
                    }
                    if (strcmp($query['email'], $_POST['InputEmail']) == 0)
                    {
                        header("Location: /register.php?err=email_already_in_use");
                        return ;
                    }
                }
                $stmt = $bdd->prepare("INSERT INTO user (email, username, password, verified, userlevel) VALUE ('".$_POST['InputEmail']."', '".$_POST['InputUsername']."', '".password_hash($_POST['InputPassword'], PASSWORD_DEFAULT)."', 0, 1);");
                $stmt->execute();

                header("Location: /index.php?msg=account_created");
                return ;
                //echo "INSERT INTO user (email, username, password, verified, userlevel) VALUE (".$_POST['InputEmail'].", ".$_POST['InputUsername'].", ".password_hash($_POST['InputPassword'], PASSWORD_DEFAULT).", 0, 1);";
            }
            else
                header("Location: /register.php?err=invalid_username");
        }
        else
            header("Location: /register.php?err=passwords_do_not_match");
    }
    else
        header("Location: /register.php?err=password_too_short");
    print_r($_POST);
}
else
    header('Location: /register.php?err=email_invalid');
?>