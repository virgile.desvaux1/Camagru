<?php
include "header.php";
include "menu.php";
?>
<title>Register</title>

<div class="register">
    <?php

    if (isset($_GET['err']))
        echo $_GET['err'];
?>
    <form action="create_account.php" method="post">
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" id="InputUsername" name="InputUsername" placeholder="Enter username">
        </div>
        <div class="form-group">
            <label>Email address</label>
            <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" id="InputPassword" name="InputPassword" placeholder="Enter password">
        </div>
        <div class="form-group">
            <label>Confirm Password</label>
            <input type="password" class="form-control" id="InputPasswordConfirm" name="InputPasswordConfirm" placeholder="Enter password again">
        </div>
        <button type="submit" class="btn btn-primary">Create Account</button>
    </form>
</div>