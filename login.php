<?php
include "header.php";
include "menu.php";
?>
<title>Login</title>

<div class="login">
    <form action="login_account.php" method="post">
        <div class="form-group">
            <label>Email address</label>
            <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" id="InputPassword" name="InputPassword" placeholder="Enter password">
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>
</div>