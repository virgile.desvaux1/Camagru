<?php
function connectDB()
{
	$DB_host = "localhost";
	$DB_user = "root";
	$DB_pass = "clafoutis";
	$DB_name = "camagru";
	$bdd = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return ($bdd);
}
?>