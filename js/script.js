
// declare global variables

let width =500,
    height =0,
    filter= "none",
    streaming=false;

// fetching elements from dom

const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const photos = document.getElementById('photos');
const clearButton = document.getElementById('clear-button');
const photoButton = document.getElementById('photo-button');
const saveButton = document.getElementById('publish-button');

// get the webcam onto the browser

navigator.mediaDevices.getUserMedia({video:true,audio:false}
)
.then(function(stream){
     video.srcObject = stream;
     video.play(); 
})
.catch(function(err){
    console.log(`Error: ${err}`);
});

// play when ready


video.addEventListener('canplay',function(e){

     if(!streaming){
        
        height = video.videoHeight / (video.videoWidth/width);

        video.setAttribute('width',width);
        video.setAttribute('height',height);
        canvas.setAttribute('width',width);
        canvas.setAttribute('height',height);

        streaming = true;

     }


},false);

// attaching event to photo button

saveButton.addEventListener('click', function(e){
    var dataURL = canvas.toDataURL();
    $.ajax({
        type: "POST",
        url: "/post_image.php",
        data: { 
           imgBase64: dataURL
        }
      }).done(function(o) {
        console.log('saved'); 
        // If you want the file to be visible in the browser 
        // - please modify the callback in javascript. All you
        // need is to return the url to the file, you just saved 
        // and than put the image in your browser.
      });
}, false);

photoButton.addEventListener('click',function(e){

   takePicture();

   e.preventDefault();


},false);

// attaching event to photo filter section


// event to clear out the photos



// function to take picture

function takePicture()
{
    const context = canvas.getContext('2d');

    if(width && height)
    {
        canvas.width = width;
        canvas.height = height;

        // draw the image of the webcam on the canvas
        drawing = new Image();
        drawing.src = document.getElementById("overlayImg").src; // can also be a remote URL e.g. http://
        drawing.onload = function() {
        context.drawImage(drawing,0,0);
        };
        context.drawImage(video,0,0,width,height);
/*
        const imgUrl = canvas.toDataURL('image/png');

        // create image element

        const img = document.createElement('img');

        // set img src

        img.setAttribute('src',imgUrl);

        img.style.filter = filter;

        photos.appendChild(img);
*/

    }
}

function overlay(element)
{
    document.getElementById("overlayImg").src = element.src;
    console.log(element);
}